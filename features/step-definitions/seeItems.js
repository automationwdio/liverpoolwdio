import { Given, When, Then } from '@wdio/cucumber-framework';

// import LoginPage from '../pageobjects/login.page';

// const pages = {
//     login: LoginPage
// }

Given(/^I am on the (\w+) login page$/, async () => {
    //await pages[page].open()
    await browser.url("https://automationteststore.com/");
    await browser.pause();
});

When(/^I can select an Makeup option from navigation bar and click$/, async () => {
    const SelectMakeup = await $("//*[@href='https://automationteststore.com/index.php?rt=product/category&path=36']");
    await (SelectMakeup).click();
    
});

Then(/^I should see results page $/, async () => {
    await browser.pause(5000);
    const Makeup = await $("///span[@innertext='Skincare']");
    expect(Makeup).toHaveTextContaining('Skincare');
    
});