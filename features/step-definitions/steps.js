import { Given, When, Then } from '@wdio/cucumber-framework';

import LoginPage from '../pageobjects/login.page';
//import ValidateText from '../pageobjects/message.page';


const pages = {
    login: LoginPage
}

Given(/^I am on the (\w+) page$/, async (page) => {
    await pages[page].open()
});

When(/^I login with (\w+) and (.+)$/, async (username, password) => {
    await LoginPage.login(username, password)
    
});

Then(/^I should see a message saying my account$/, async () => {
    await browser.pause(5000);
      const myAccount = await $("//span[@innertext='My Account']");
      expect(myAccount).toHaveTextContaining("My Account");
 });

