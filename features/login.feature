Feature: The Internet Guinea Pig Website

  Scenario Outline: As a user, I can log into automation test store page

    Given I am on the login page
    When I login with <username> and <password>
    Then I should see a message saying my account

    Examples:
      | username | password             | 
      | testaut  | testauto123          | 

