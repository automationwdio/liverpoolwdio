

import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
     open() {
        return super.open("index.php?rt=account/login");
    }

    get inputUsername () {
        return $("//*[@id='loginFrm_loginname']");
    }

    get inputPassword () {
        return $("//*[@id='loginFrm_password']");
    }

    get btnSubmit () {
        return $('button[title="Login"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login (username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    
}

export default new LoginPage();
