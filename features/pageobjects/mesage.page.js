

import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ValidateText extends Page {
    /**
     * define selectors using getter methods
     */
    get text () {
        return $("//h1//span[@innerText=' My Account']");
    }
}

export default new ValidateText();
